﻿using CK.ConveyorBelt.Common;
using UnityEngine;

namespace CK.ConveyorBelt.Package
{
    public class PackageSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject _package;
        [SerializeField] private Transform _location;

        private float _length = 0.3f;
        private float _width = 0.3f;

        private float _time;

        private const float Rate = 1;

        internal void Awake()
        {
            _time = Rate;
        }

        public void UpdateLength( float length )
        {
            _length = length / 100f;
        }

        public void UpdateWidth( float width )
        {
            _width = width / 100f;
        }

        internal void FixedUpdate()
        {
            if( !( Time.time > _time ) )
                return;

            _time = Time.time + Rate;

            var instantiated = Instantiate( _package, _location, false );
            instantiated.transform.localScale = new Vector3( _length, 0.6f, _width );
            instantiated.transform.position = instantiated.transform.position.WithY( 0.3f );
        }
    }
}
﻿using UnityEngine;

namespace CK.ConveyorBelt.Package
{
    public class PackageManager : MonoBehaviour
    {
        [SerializeField] private float _speed = 6;

        internal void OnBecameInvisible()
        {
            Destroy( gameObject );
        }

        internal void FixedUpdate()
        {
            transform.Translate( Vector3.forward * _speed / 3.6f * Time.fixedDeltaTime );
        }
    }
}
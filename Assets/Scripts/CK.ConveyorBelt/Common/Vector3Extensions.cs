﻿using UnityEngine;

namespace CK.ConveyorBelt.Common
{
    public static class Vector3Extensions
    {
        public static Vector3 WithY( this Vector3 @this, float y )
        {
            return new Vector3( @this.x, y, @this.z );
        }
    }
}
﻿using CK.ConveyorBelt.Common;
using UnityEngine;

namespace CK.ConveyorBelt.Camera
{
    [RequireComponent( typeof( UnityEngine.Camera ) )]
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Camera _camera;

        internal void Reset()
        {
            _camera = GetComponent<UnityEngine.Camera>();
        }

        public void UpdateHeight( float height )
        {
            transform.position = transform.position.WithY( height / 100 );
        }

        public void UpdateFoV( float fov )
        {
            _camera.fieldOfView = fov;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace CK.ConveyorBelt.UI
{
    [RequireComponent( typeof( Text ) )]
    public class SliderWatcher : MonoBehaviour
    {
        [SerializeField] private Text _textComponent;
        [SerializeField] private string _text;
        [SerializeField] private string _conclusion;

        internal void Reset()
        {
            _textComponent = GetComponent<Text>();
        }

        internal void Awake()
        {
            _textComponent.text = _text;
        }

        public void UpdateText( float scrollValue )
        {
            _textComponent.text = string.Format( "{0} {1} {2}", _text, scrollValue, _conclusion );
        }
    }
}